package bench

import io.circe.{Decoder, HCursor, Json}

case class SimpleMsg(data: Json, meta: Option[Json] = None)


object SimpleMsg {


  implicit val decSM: Decoder[SimpleMsg] = (o: HCursor) => {
    for {
      data <- o.downField("data").as[Json]
      meta <- o.downField("meatdata").as[Option[Json]]
    } yield SimpleMsg(data, meta)
  }

}
