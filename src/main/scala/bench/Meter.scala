package bench

import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference

import bench.SimpleMsg.decSM
import cats.Monad
import io.circe.syntax._
import io.circe.parser._
import cats.data.Chain
import com.iqoption.analytics.kafka.message._
import io.circe.{Decoder, HCursor, Json, jawn}
import org.openjdk.jmh.annotations._

import scala.collection.concurrent.TrieMap
import scala.collection.mutable
import io.circe.parser._
import io.circe.syntax._

import scala.concurrent.Future

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Thread)
class Meter {

  //  def str() = """{"a":"b","c":1,"d":{"v":"r"},"z":["a","b"]}"""
  //
  //  val json = parse(str()).getOrElse(Json.Null)
  //
  //  case class CopyMe(s: String, c: Int, d: HelpMe, z: Seq[String])
  //  case class HelpMe(v: String)
  //
  //  implicit val decoder1: Decoder[HelpMe] = (o: HCursor) => {
  //    o.downField("v").as[String].map(HelpMe)
  //  }
  //
  //  implicit val decoder2: Decoder[CopyMe] = (o: HCursor) => {
  //    for {
  //      a <- o.downField("a").as[String]
  //      c <- o.downField("c").as[Int]
  //      d <- o.downField("d").as[HelpMe]
  //      z <- o.downField("z").as[Seq[String]]
  //    } yield
  //      CopyMe(
  //        a,
  //        c,
  //        d,
  //        z
  //      )
  //  }

  import scala.concurrent.ExecutionContext.global

  //  @Benchmark
  //  def parseJson = parse(str())
  //
  //  @Benchmark
  //  def dec1 = json.as[CopyMe]

  //  @Benchmark
  //  def decFullDecoder = decode[CopyMe](str()).getOrElse(Json.Null)
  //
  //  @Benchmark
  //  def decFullParser = parse(str()).map(_.as[CopyMe]).getOrElse(Json.Null)
  //
  //  val seq = 1.to(1000)
  //  val add = 1.to(3)
  //
  //  val seqList = seq.toList
  //  val addList = add.toList
  //
  //  val seqVector = seq.toVector
  //  val addVector = add.toVector
  //
  //  val filter = 1001.to(2000).toSet
  //  import scala.concurrent.ExecutionContext.Implicits._
  //  @Benchmark
  //  def mergeViaTemp = {
  //    var temp = seq.toList
  //    add.foreach(el => temp = el +: temp)
  //    temp
  //  }
  //
  //  @Benchmark
  //  def list = {
  //    seq.toList
  //    add.toList
  //  }
  //
  //  @Benchmark
  //  def mergeVia2Lists = {
  //    val a = add.toList
  //    val b = seq.toList
  //    a ++ b
  //  }

  //  import scalqa._
  //
  //  @Benchmark
  //  def t2 = {
  //    var a: Array[Int] = Array.emptyIntArray
  //    1.to(10000).foreach(i => a = i +: a)
  //    println(a.head)
  //    a.reverse
  //  }
  //
  //  @Benchmark
  //  def t3 = {
  //    var a: Refs[Int] = \/
  //    1.to(10000).foreach(i => a += i)
  //    println(a.head)
  //    a
  //  }

  import scala.language.higherKinds
  import cats.Monad
  import cats.syntax.functor._
  import cats.syntax.flatMap._
  import cats.Id

  val kafkaMessage = {
    val s =
      """{"data":{"data":{"last_change_reason":"market_order","instrument_id_escape":"doGBPUSD-OTC201903170959PT1MPSPT","instrument_strike_value":1225115,"instrument_type":"digital-option","instrument_id":"doGBPUSD-OTC201903170959PT1MPSPT","instrument_underlying":"GBPUSD-OTC","instrument_active_id":81,"instrument_expiration":1552816740000,"instrument_strike":1.225115,"instrument_dir":"put","instrument_period":60,"instrument_percent":null,"id":696032832,"user_id":43803549,"user_balance_id":207880121,"user_balance_type":4,"create_at":1552816677204,"update_at":1552816729326,"close_at":1552816729326,"type":"long","leverage":1,"count":0.0,"count_realized":0.136581,"status":"closed","buy_amount":6.99998,"sell_amount":0.136581,"buy_amount_enrolled":6.99998,"sell_amount_enrolled":0.136581,"commission_amount":0.0,"commission":0.0,"commission_enrolled":0.0,"close_effect_amount":0.0,"close_effect_amount_enrolled":0.0,"close_reason":"default","close_underlying_price":1.225197,"open_underlying_price":1.225115,"pnl_realized":-6.863399,"pnl_realized_enrolled":-6.863399,"buy_avg_price":51.251491,"buy_avg_price_enrolled":51.251491,"sell_avg_price":1.0,"sell_avg_price_enrolled":1.0,"stop_lose_order_id":null,"take_profit_order_id":null,"tpsl_extra":null,"currency":"USD","margin":0.0,"margin_call":0.0,"margin_call_enrolled":0.0,"swap":0.0,"swap_enrolled":0.0,"custodial":0.0,"custodial_enrolled":0.0,"custodial_last_age":0,"user_group_id":1,"charge":0.0,"charge_enrolled":0.0,"orders":[],"currency_unit":1,"currency_rate":1.0,"open_client_platform_id":8,"extra_data":{"amount":7000000,"version":"4.0","init_time":"1552816729298","spot_option":true,"use_trail_stop":false,"auto_margin_call":false,"last_change_reason":"market_order","lower_instrument_id":"doGBPUSD-OTC201903170959PT1MP12251","upper_instrument_id":"doGBPUSD-OTC201903170959PT1MP122512","lower_instrument_strike":1225100,"upper_instrument_strike":1225120,"use_token_for_commission":false},"order_ids":[1239329476,1239328362],"last_index":5580684420,"index":5580684420},"metadata":null},"meta":{"messageType":"IQBusEvent","index":5580684420,"initiatorMessageInfo":{"service":"trading","name":"position-changed","version":"1.0","receivedTime":1552816729343},"prevIndex":null,"receivedTime":1552816729366,"prevOffset":1037132194,"prevTopic":"iqbus_trading-position-changed-1_0-raw","traceId":null},"info":{"iqbusCreated":null,"fromOffset":null,"balancerStarted":null,"fetchStarted":null,"delivered":null,"fetchFailure":null,"fetchTries":0},"version":"4.0"}"""
    ByteBuffer.wrap(s.getBytes)
  }

  val simplified= {
    val s = """{"data":{"last_change_reason":"market_order","instrument_id_escape":"doGBPUSD-OTC201903170959PT1MPSPT","instrument_strike_value":1225115,"instrument_type":"digital-option","instrument_id":"doGBPUSD-OTC201903170959PT1MPSPT","instrument_underlying":"GBPUSD-OTC","instrument_active_id":81,"instrument_expiration":1552816740000,"instrument_strike":1.225115,"instrument_dir":"put","instrument_period":60,"instrument_percent":null,"id":696032832,"user_id":43803549,"user_balance_id":207880121,"user_balance_type":4,"create_at":1552816677204,"update_at":1552816729326,"close_at":1552816729326,"type":"long","leverage":1,"count":0.0,"count_realized":0.136581,"status":"closed","buy_amount":6.99998,"sell_amount":0.136581,"buy_amount_enrolled":6.99998,"sell_amount_enrolled":0.136581,"commission_amount":0.0,"commission":0.0,"commission_enrolled":0.0,"close_effect_amount":0.0,"close_effect_amount_enrolled":0.0,"close_reason":"default","close_underlying_price":1.225197,"open_underlying_price":1.225115,"pnl_realized":-6.863399,"pnl_realized_enrolled":-6.863399,"buy_avg_price":51.251491,"buy_avg_price_enrolled":51.251491,"sell_avg_price":1.0,"sell_avg_price_enrolled":1.0,"stop_lose_order_id":null,"take_profit_order_id":null,"tpsl_extra":null,"currency":"USD","margin":0.0,"margin_call":0.0,"margin_call_enrolled":0.0,"swap":0.0,"swap_enrolled":0.0,"custodial":0.0,"custodial_enrolled":0.0,"custodial_last_age":0,"user_group_id":1,"charge":0.0,"charge_enrolled":0.0,"orders":[],"currency_unit":1,"currency_rate":1.0,"open_client_platform_id":8,"extra_data":{"amount":7000000,"version":"4.0","init_time":"1552816729298","spot_option":true,"use_trail_stop":false,"auto_margin_call":false,"last_change_reason":"market_order","lower_instrument_id":"doGBPUSD-OTC201903170959PT1MP12251","upper_instrument_id":"doGBPUSD-OTC201903170959PT1MP122512","lower_instrument_strike":1225100,"upper_instrument_strike":1225120,"use_token_for_commission":false},"order_ids":[1239329476,1239328362],"last_index":5580684420,"index":5580684420},"metadata":null}"""
    ByteBuffer.wrap(s.getBytes)
  }

  val kakfkaMessageUC = {
    val s ="""{"data":{"data":{"user_id":21137029,"first_name":"JIRAPA","last_name":"????????SAIKAMON","nickname":"Nevaeh White","is_public":false,"user_name":"JIRAPA ?.","locale":"en_US","platform_id":8,"connection_hash":"11761907031568540770","ip":"58.10.107.16","connected_timestamp":1552812811930,"country_id":194,"group_id":1,"brand_id":1},"metadata":{}},"meta":{"messageType":"IQBusEvent","index":-1,"initiatorMessageInfo":{"service":"ws","name":"user-connection-checked","version":"1.0","receivedTime":1552818126979},"prevIndex":null,"receivedTime":1552818126979,"prevOffset":null,"prevTopic":null,"traceId":null},"info":{"iqbusCreated":null,"fromOffset":null,"balancerStarted":null,"fetchStarted":null,"delivered":null,"fetchFailure":null,"fetchTries":0},"version":"4.0"}"""
    ByteBuffer.wrap(s.getBytes)
  }

  val simpleUC = {
    val s ="""{"data":{"user_id":21137029,"first_name":"JIRAPA","last_name":"????????SAIKAMON","nickname":"Nevaeh White","is_public":false,"user_name":"JIRAPA ?.","locale":"en_US","platform_id":8,"connection_hash":"11761907031568540770","ip":"58.10.107.16","connected_timestamp":1552812811930,"country_id":194,"group_id":1,"brand_id":0},"metadata":{}}"""
    ByteBuffer.wrap(s.getBytes)
  }

  def decode(buffer: ByteBuffer) = {
    val json = jawn.parseByteBuffer(buffer)
//  println("JSON", json, json match {
//    case Right(v) => "ok"
//    case Left(e) => s"${e.getMessage()}:: ${e.underlying.getStackTraceString}"
//  })
    val eventMessage =
      json
        .map {
          _.as[KEventMessageOpt](KEventMessageOpt.decoder).map { ev =>
            //     println(ev.data)
            KEventMessage(ev.data.get, ev.meta, ev.info)
          }
        }
    eventMessage match {
      case Left(e) => throw e
      case _       =>
    }
  }

  def decodeCI(buffer: ByteBuffer) = {
    val str = new String(buffer.array(), StandardCharsets.UTF_8)
    val eventMessage = io.circe.parser
      .parse(str)
      .flatMap(_.as[KEventMessageOpt])
      .map { ev =>
          KEventMessage(ev.data.get, ev.meta, ev.info)
      }
    eventMessage match {
      case Left(e) => throw e
      case _       =>
    }
  }
  def decodeCISimple(buffer: ByteBuffer) = {
    val str = new String(buffer.array(), StandardCharsets.UTF_8)
    val eventMessage = io.circe.parser
      .parse(str)
      .flatMap {
        _.as[SimpleMsg](decSM)
      }
    eventMessage  match {
      case Left(e) => throw e
      case _       =>
    }
  }


  def decodeSimple(buffer: ByteBuffer) = {
    import SimpleMsg.decSM
    val json = jawn.parseByteBuffer(buffer)

    val eventMessage =
      json
        .flatMap {
          _.as[SimpleMsg](decSM)
        }
    eventMessage  match {
      case Left(e) => throw e
      case _       =>
    }
  }
//
  val seq = 1.to(100).toList
//
//  val seqTEShort = 101.to(200).toList.map { v =>
//    val bytes = simplified(v).getBytes()
//    ByteBuffer.wrap(bytes)
//  }
//
//  val seqUC = 1.to(100).toList.map { v =>
//    val bytes = kakfkaMessageUC(v).getBytes()
//    ByteBuffer.wrap(bytes)
//  }
//
//  val seqUCShort = 101.to(200).toList.map { v =>
//    val bytes = simpleUC(v).getBytes()
//    ByteBuffer.wrap(bytes)
//  }

  @Benchmark
  def seqTEBench = {
    seq.foreach(_ => decode(kafkaMessage.position(0)))
  }

  @Benchmark
  def seqTECIBench = {
    seq.foreach(_ =>decodeCI(kafkaMessage.position(0)))
  }

  @Benchmark
  def seqTECISimpleBench = {
    seq.foreach(_ =>decodeCISimple(simplified.position(0)))
  }

  @Benchmark
  def seqTESimpleBench = {
    seq.foreach(_ =>decodeSimple(simplified.position(0)))
  }

  @Benchmark
  def seqUCBench = {
    seq.foreach(_ =>decode(kakfkaMessageUC.position(0)))
  }

  @Benchmark
  def seqUCSimpleBench = {
    seq.foreach(_ =>decodeSimple(simpleUC.position(0)))
  }

  @Benchmark
  def seqUCCIBench = {
    seq.foreach(_ =>decodeCI(kakfkaMessageUC.position(0)))
  }

  @Benchmark
  def seqUCCISimpleBench = {
    seq.foreach(_ =>decodeCISimple(simpleUC.position(0)))
  }
}
