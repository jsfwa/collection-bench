name := "catstest"

version := "0.1"

scalaVersion := "2.12.8"

resolvers += "Sonatype OSS Snapshots" at
  "https://oss.sonatype.org/content/repositories/releases"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core" % "1.3.1",
  "org.scalactic" %% "scalactic" % "3.0.4",
  "org.scalatest" %% "scalatest" % "3.0.4" % "test",
  "com.iqoption.analytics" % "iqkafka-model_2.12"     % "0.0.7",
)

val circeVersion = "0.9.3"

libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)

libraryDependencies += "org.typelevel" %% "cats-effect" % "1.0.0"

enablePlugins(JmhPlugin)

unmanagedJars in Compile += file("src/lib2/scalqa-core_2.12-0.25.jar")